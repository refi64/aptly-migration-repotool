package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"github.com/aptly-dev/aptly/deb"
	"github.com/aptly-dev/aptly/pgp"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/filter"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"github.com/syndtr/goleveldb/leveldb/util"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func editRelease(path string, p deb.PublishedRepo, gpg *pgp.GpgSigner, apply bool) {
	fmt.Println(path)

	releaseDir := filepath.Join("/aptly/data/public/apertis/dists", path)
	releasePath := filepath.Join(releaseDir, "Release")
	releaseTempPath := releasePath + ".tmp"

	inreleasePath := filepath.Join(releaseDir, "InRelease")
	inreleaseTempPath := inreleasePath + ".tmp"

	releaseGpgPath := releasePath + ".gpg"
	releaseGpgTempPath := releaseTempPath + ".gpg"

	releaseFile, err := os.Open(releasePath)
	if err != nil {
		if strings.HasPrefix(path, "v2022/snapshots/") && errors.Is(err, os.ErrNotExist) {
			fmt.Printf("%s does not exist! skipping...\n", path)
			return
		}

		log.Fatal("open release: ", path, err)
	}
	defer releaseFile.Close()

	reader := deb.NewControlFileReader(releaseFile, true, false)
	stanza, err := reader.ReadStanza()
	if err != nil {
		log.Fatal("read release: ", path, err)
	}

	stanza["Origin"] = p.Origin
	stanza["Label"] = p.Label

	releaseTempFile, err := os.Create(releaseTempPath)
	if err != nil {
		log.Fatal("open release.tmp: ", path, err)
	}
	defer releaseTempFile.Close()
	bw := bufio.NewWriter(releaseTempFile)
	if err := stanza.WriteTo(bw, false, true, false); err != nil {
		log.Fatal("write release.tmp: ", path, err)
	}
	if err := bw.Flush(); err != nil {
		log.Fatal("flusuh release.tmp: ", path, err)
	}

	if err := gpg.DetachedSign(releaseTempPath, releaseGpgTempPath); err != nil {
		log.Fatal("gpg release: ", path, err)
	}

	if err := gpg.ClearSign(releaseTempPath, inreleaseTempPath); err != nil {
		log.Fatal("gpg inrelease: ", path, err)
	}

	if apply {
		if err := os.Rename(releaseTempPath, releasePath); err != nil {
			log.Fatal("rename: ", releasePath, err)
		}
		if err := os.Rename(inreleaseTempPath, inreleasePath); err != nil {
			log.Fatal("rename in: ", releasePath, err)
		}
		if err := os.Rename(releaseGpgTempPath, releaseGpgPath); err != nil {
			log.Fatal("rename gpg: ", releaseGpgPath, err)
		}
	}
}

func main() {
	var apply bool
	flag.BoolVar(&apply, "apply", false, "actually apply")
	flag.Parse()

	db, err := leveldb.OpenFile("/aptly/data/db", &opt.Options{
		Filter:                 filter.NewBloomFilter(10),
		OpenFilesCacheCapacity: 256,
	})
	if err != nil {
		log.Fatal("open db:", err)
	}
	defer db.Close()

	changedPublishes := map[string]deb.PublishedRepo{}
	batch := new(leveldb.Batch)

	const prefix = "Uapertis>>"
	iter := db.NewIterator(util.BytesPrefix([]byte(prefix)), nil)
	for iter.Next() {
		path := string(iter.Key())[len(prefix):]

		dist := strings.Split(path, "/")[0]
		distBase := dist[:5]

		var p deb.PublishedRepo
		if err := p.Decode(iter.Value()); err != nil {
			log.Fatal("decode key: ", string(path), err)
		}

		isReprepro := false
		if distBase == "v2023" {
			if distBase != dist && !strings.HasPrefix(dist, "v2023-") && strings.Compare(dist, "v2023dev3") < 0 {
				isReprepro = true
			}
		} else if strings.Compare(distBase, "v2023") < 0 {
			isReprepro = true
		}

		newOrigin := ""
		newLabel := ""
		if isReprepro {
			newOrigin = "Apertis"
			newLabel = fmt.Sprintf("Apertis %s repository", dist)
		} else {
			newOrigin = fmt.Sprintf("shared/apertis/public/apertis %s", path)
			newLabel = newOrigin
		}

		if newOrigin != p.Origin || newLabel != p.Label {
			fmt.Printf("CHANGE: %s [%s] [%s]\n", path, newOrigin, newLabel)
			p.Origin = newOrigin
			p.Label = newLabel
			batch.Put(iter.Key(), p.Encode())
			changedPublishes[path] = p
		}
	}

	iter.Release()
	if err := iter.Error(); err != nil {
		log.Fatal("iter: ", err)
	}

	gpg := pgp.NewGpgSigner(pgp.GPGDefaultFinder())
	gpg.SetKey("F5022C4EEB5691FC")

	for path, p := range changedPublishes {
		editRelease(path, p, gpg, apply)
	}

	if apply {
		if err := db.Write(batch, nil); err != nil {
			log.Fatal("write batch: ", err)
		}
	}
}
