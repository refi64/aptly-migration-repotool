#!/usr/bin/env tclsh

package require tepam
package require tanuki

tepam::procedure {tool push} {} {
  set ::env(GOARCH) amd64
  tanuki::shell::run {go build ./...}
  tanuki::shell::run {kubectl -n aptly-repositories cp dbsurgeon aptly-0:/usr/local/bin}
}

tepam::procedure {tool run} {-args {
  {-u -type none}
  {-apply -type none}
  {_args -multiple -default {}}
}} {
  if {$u} {
    tool push
  }
  tanuki::shell::run {kubectl -n aptly-repositories exec -it aptly-0 -- \
    dbsurgeon {*}[tanuki::maybeflags apply] {*}$_args}
}

cd dbsurgeon
tool {*}$::argv
