#!/usr/bin/env python3

from bs4 import BeautifulSoup
from debian.deb822 import Packages, Sources, Release
from debian.debian_support import NativeVersion

from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor, wait
from dataclasses import dataclass
from pathlib import Path
from tempfile import TemporaryFile
from typing import Annotated, Any, Callable, Generic, IO, Iterator, Optional, TypeVar

import collections
import contextlib
import fnmatch
import gzip
import itertools
import os
import re
import requests
import shlex
import subprocess
import sys
import time
import typer

try:
    import apt_pkg  # type: ignore
except ImportError:
    use_apt_pkg = False
else:
    use_apt_pkg = True


DEFAULT_APTLY_API_URL = 'https://manual-api.aptly-test.apertis.dev/'
DEFAULT_APTLY_REPO_URL = 'https://manual.aptly-test.apertis.dev'
DEFAULT_APTLY_GPG_KEY = '0BC0002B26BAFF8F'

# Keep non-release snapshots after this cutoff date.
SNAPSHOTS_CUTOFF = '20230701T000000Z'

# BASE_URL = 'https://repositories.apertis.org'
BASE_URL = 'https://appdev.apertis.org'

REQUESTS_SESSION = requests.Session()

ROOT_DIR = Path(__file__).resolve().parent


@dataclass
class DistEntry:
    APERTIS_GROUP = 'apertis'

    group: str
    dist: str

    @staticmethod
    def parse(dist: str) -> 'DistEntry':
        if '/' in dist:
            group, dist = dist.split('/')
            return DistEntry(group, dist)
        else:
            return DistEntry(DistEntry.APERTIS_GROUP, dist)

    @property
    def path(self) -> str:
        return f'{self.group}/{self.dist}'

    def __str__(self) -> str:
        return self.path


def list_dist_entries() -> list[DistEntry]:
    return [
        DistEntry.parse(line)
        for line in (ROOT_DIR / 'dists.txt').read_text().splitlines()
    ]


def get_dist_snapshots_path(dist: DistEntry) -> Path:
    assert dist.group == DistEntry.APERTIS_GROUP
    return (ROOT_DIR / 'snapshots' / dist.dist).with_suffix('.txt')


def list_dist_snapshots(dist: DistEntry) -> list[str]:
    return get_dist_snapshots_path(dist).read_text().splitlines()


def is_404(ex: requests.HTTPError) -> bool:
    return ex.response.status_code == 404


DownloadTransformer = Callable[[bytes], bytes]


def download_content(
    url: str,
    *,
    transform: Optional[DownloadTransformer] = None,
) -> bytes:
    r = REQUESTS_SESSION.get(url)
    r.raise_for_status()

    if transform is not None:
        return transform(r.content)
    else:
        return r.content


class MultiDownload404(Exception):
    def __init__(self, failed_urls: list[str]) -> None:
        self.failed_urls = failed_urls
        super(MultiDownload404, self).__init__(f'404: {" ".join(self.failed_urls)}')


def download_multi_handle_404(
    *urls: str,
    transform: Optional[DownloadTransformer] = None,
) -> list[bytes]:
    results = []
    failed_urls = []

    for url in urls:
        try:
            content = download_content(url, transform=transform)
        except requests.HTTPError as ex:
            if is_404(ex):
                failed_urls.append(url)
        else:
            if not failed_urls:
                results.append(content)

    if failed_urls:
        raise MultiDownload404(failed_urls)

    return results


@dataclass
class RemoteResource:
    name: str
    url: str


def download_dist_snapshots_list(dist: DistEntry) -> list[RemoteResource]:
    snapshots = []
    snapshots_url = f'{BASE_URL}/{dist.group}/dists/{dist.dist}/snapshots'

    try:
        page = download_content(snapshots_url)
    except requests.HTTPError as ex:
        if is_404(ex):
            print(f'WARNING: {ex.request.url} snapshots returned 404, skipping')
            return []
        else:
            raise

    soup = BeautifulSoup(page, 'html.parser')

    for node in soup.select('td a'):
        if (href := node.attrs.get('href')) is not None and re.match(
            r'^\d{8}T\d{6}Z/$', href
        ) is not None:
            snapshot = href.rstrip('/')
            snapshots.append(
                RemoteResource(name=snapshot, url=f'{snapshots_url}/{snapshot}')
            )

    return snapshots


def download_dist_releases_list(dist: DistEntry) -> list[RemoteResource]:
    assert dist.group == DistEntry.APERTIS_GROUP

    released_dist = dist.dist.split('-')[0]

    releases = []
    releases_url = f'https://images.apertis.org/release/{released_dist}'

    try:
        page = download_content(releases_url)
    except requests.HTTPError as ex:
        if is_404(ex):
            print(f'WARNING: {ex.request.url} release returned 404, skipping')
            return []
        else:
            raise

    soup = BeautifulSoup(page, 'html.parser')

    for node in soup.select('pre a'):
        if (
            (href := node.attrs.get('href')) is not None
            and href.startswith(released_dist)
            and href.endswith('/')
        ):
            release = href.rstrip('/')
            releases.append(
                RemoteResource(name=release, url=f'{releases_url}/{release}')
            )

    return releases


_T = TypeVar('_T')
_U = TypeVar('_U')


@dataclass
class SetDiff(Generic[_T]):
    same: set[_T]
    added: set[_T]
    removed: set[_T]

    @staticmethod
    def compute(a: set[_T], b: set[_T]) -> 'SetDiff[_T]':
        return SetDiff(same=a & b, added=b - a, removed=a - b)

    @property
    def changed(self) -> bool:
        return bool(self.added or self.removed)

    def map(self, func: Callable[[_T], _U]) -> 'SetDiff[_U]':
        return SetDiff(
            same={func(x) for x in self.same},
            added={func(x) for x in self.added},
            removed={func(x) for x in self.removed},
        )


class DiffReporter:
    def __init__(self, io: IO[str]) -> None:
        self._indent = 0
        self._io = io
        self._alerts = 0

    @contextlib.contextmanager
    def indent(self) -> Iterator[None]:
        self._indent += 1
        try:
            yield
        finally:
            self._indent -= 1

    def print(self, *args: Any) -> None:
        print((self._indent * 2) * ' ', *args, sep='', file=self._io)

    def process_diff(
        self,
        diff: SetDiff[str],
        *,
        alert_on_changes: bool = True,
    ) -> None:
        if diff.changed:
            if alert_on_changes:
                self.print('ALERT: unexpected changes found!')
                self.add_alert()
            else:
                self.print('changes found')

            self.print(f'{len(diff.same)} identical')
            if diff.same:
                with self.indent():
                    self.print(' '.join(diff.same))

            self.print(f'{len(diff.removed)} removed')
            if diff.removed:
                with self.indent():
                    self.print(' '.join(diff.removed))

            self.print(f'{len(diff.added)} added')
            if diff.added:
                with self.indent():
                    self.print(' '.join(diff.added))
        else:
            self.print('No changes.')

    @property
    def alerts(self) -> int:
        return self._alerts

    def add_alert(self) -> None:
        self._alerts += 1


@dataclass(frozen=True)
class PackageFile:
    name: str
    size: int
    sha256: str


@dataclass(frozen=True)
class PackageMeta:
    name: str
    version: str
    files: frozenset[PackageFile]


def diff_package_meta(
    reporter: DiffReporter,
    meta_1: set[PackageMeta],
    meta_2: set[PackageMeta],
) -> None:
    def unordered_group_meta(meta: set[PackageMeta]) -> dict[str, set[PackageMeta]]:
        result = collections.defaultdict(lambda: set())
        for item in meta:
            result[item.name].add(item)
        return result

    by_name_1, by_name_2 = [unordered_group_meta(meta) for meta in (meta_1, meta_2)]
    packages_names_diff = SetDiff.compute(
        set(by_name_1.keys()),
        set(by_name_2.keys()),
    )
    reporter.print('Package names:')
    with reporter.indent():
        reporter.process_diff(packages_names_diff)

    for name in sorted(packages_names_diff.same):
        by_version_1, by_version_2 = [
            {p.version: p for p in by_name[name]} for by_name in (by_name_1, by_name_2)
        ]

        versions_diff = SetDiff.compute(
            set(by_version_1.keys()),
            set(by_version_2.keys()),
        )
        if versions_diff.changed:
            reporter.print(f'{name}:')
            with reporter.indent():
                has_newest = False
                if not versions_diff.added:
                    parsed_versions_1, parsed_versions_2 = [
                        [NativeVersion(v) for v in versions]
                        for versions in (by_version_1, by_version_2)
                    ]
                    newest_version = max(
                        itertools.chain(parsed_versions_1, parsed_versions_2)
                    )
                    if newest_version in parsed_versions_2:
                        has_newest = True

                if has_newest:
                    reporter.process_diff(versions_diff, alert_on_changes=False)
                    reporter.print('Right-hand-side has the newest version')
                else:
                    reporter.process_diff(versions_diff)

        for version in versions_diff.same:
            files_diff = SetDiff.compute(
                set(by_version_1[version].files),
                set(by_version_2[version].files),
            )
            if files_diff.changed:
                reporter.print(f'{name}@{version}:')
                with reporter.indent():
                    reporter.process_diff(
                        files_diff.map(lambda f: f'{f.name} {f.size} {f.sha256}')
                    )


def diff_packages(
    reporter: DiffReporter,
    *,
    url_1: str,
    url_2: str,
    component: str,
    arch: str,
) -> None:
    try:
        data_1, data_2 = download_multi_handle_404(
            *(f'{url}/{component}/binary-{arch}/Packages.gz' for url in (url_1, url_2)),
            transform=gzip.decompress,
        )
    except MultiDownload404 as ex:
        if len(ex.failed_urls) == 1:
            reporter.print(f'ALERT: {ex.failed_urls[0]} is 404')
            reporter.add_alert()
        else:
            reporter.print('Not found in either location, skipping')

        return

    with TemporaryFile() as fp_1, TemporaryFile() as fp_2:
        for (fp, data) in [(fp_1, data_1), (fp_2, data_2)]:
            fp.write(data)
            fp.seek(0)

        meta_1, meta_2 = [
            {
                PackageMeta(
                    name=p['package'],
                    version=p['version'],
                    files=frozenset(
                        {
                            PackageFile(
                                name=p['filename'],
                                size=int(p['size']),
                                sha256=p['sha256'],
                            )
                        }
                    ),
                )
                for p in Packages.iter_paragraphs(fp, use_apt_pkg=use_apt_pkg)
            }
            for fp in (fp_1, fp_2)
        ]

    diff_package_meta(reporter, meta_1, meta_2)


def diff_sources(
    reporter: DiffReporter,
    *,
    url_1: str,
    url_2: str,
    component: str,
) -> None:
    try:
        data_1, data_2 = download_multi_handle_404(
            *(f'{url}/{component}/source/Sources.gz' for url in (url_1, url_2)),
            transform=gzip.decompress,
        )
    except MultiDownload404 as ex:
        if len(ex.failed_urls) == 1:
            reporter.print(f'ALERT: {ex.failed_urls[0]} is 404')
            reporter.add_alert()
        else:
            reporter.print('Not found in either location, skipping')

        return

    with TemporaryFile() as fp_1, TemporaryFile() as fp_2:
        for (fp, data) in [(fp_1, data_1), (fp_2, data_2)]:
            fp.write(data)
            fp.seek(0)

        meta_1, meta_2 = [
            {
                PackageMeta(
                    name=s['package'],
                    version=s['version'],
                    files=frozenset(
                        {
                            PackageFile(
                                name=p['name'],
                                size=int(p['size']),
                                sha256=p['sha256'],
                            )
                            for p in s['checksums-sha256']
                        }
                    ),
                )
                for s in Sources.iter_paragraphs(fp, use_apt_pkg=use_apt_pkg)
            }
            for fp in (fp_1, fp_2)
        ]

    diff_package_meta(reporter, meta_1, meta_2)


def diff_dists(reporter: DiffReporter, url_1: str, url_2: str) -> None:
    releases = [Release(download_content(f'{url}/Release')) for url in (url_1, url_2)]

    component_arches_1, component_arches_2 = [
        set(itertools.product(r['components'].split(), r['architectures'].split()))
        for r in releases
    ]
    component_arches_diff = SetDiff.compute(component_arches_1, component_arches_2)

    reporter.print(f'URL 1: {url_1}')
    reporter.print(f'URL 2: {url_2}')

    reporter.print('Component/arch pairs:')
    with reporter.indent():
        reporter.process_diff(component_arches_diff.map(lambda x: f'{x[0]}/{x[1]}'))

    for (component, c_a) in itertools.groupby(
        component_arches_diff.same, key=lambda x: x[0]
    ):
        arches = [a for _, a in c_a]

        for component_with_installer in (component, f'{component}/debian-installer'):
            for arch in arches:
                reporter.print(f'{component_with_installer}/{arch}:')
                with reporter.indent():
                    diff_packages(
                        reporter,
                        url_1=url_1,
                        url_2=url_2,
                        component=component_with_installer,
                        arch=arch,
                    )

        reporter.print(f'{component}/source:')
        with reporter.indent():
            diff_sources(reporter, url_1=url_1, url_2=url_2, component=component)

    if reporter.alerts:
        reporter.print(f'ALERT: {reporter.alerts} alert(s) found!')
    else:
        reporter.print('No alerts found.')


def task_diff_dists_to_file(
    *, heading: str, path: Path, url_1: str, url_2: str
) -> None:
    start = time.monotonic()

    tmp = path.with_suffix(path.suffix + '.tmp')
    tmp.unlink(missing_ok=True)

    with tmp.open('w') as fp:
        reporter = DiffReporter(fp)
        diff_dists(reporter, url_1, url_2)

    tmp.rename(path)

    end = time.monotonic()

    if reporter.alerts:
        print(f'{heading}: {reporter.alerts} alert(s) found!')
    else:
        print(f'{heading}: No alerts found.')
    print(f'{heading}: Processed diff in {end - start:.1f}s.')


def setup_apt2aptly_env() -> None:
    if 'APTLY_API_URL' not in os.environ:
        if 'APTLY_API_TOKEN' not in os.environ:
            sys.exit('APTLY_API_TOKEN not set')

        os.environ['APTLY_API_URL'] = DEFAULT_APTLY_API_URL


def run_apt2aptly(
    *,
    dist: DistEntry,
    dry_run: bool,
    skip_snapshots: bool = False,
) -> subprocess.CompletedProcess[bytes]:
    env = os.environ.copy()

    if 'APTLY_API_URL' not in env:
        if 'APTLY_API_TOKEN' not in env:
            sys.exit('APTLY_API_TOKEN not set')

        env['APTLY_API_URL'] = DEFAULT_APTLY_API_URL

    gpg_key = env.get('APTLY_GPG_KEY', DEFAULT_APTLY_GPG_KEY)

    if dist.group == DistEntry.APERTIS_GROUP:
        aptly_repo = f'apertis:{dist.dist.replace("-", ":")}:{{component}}'
        snapshots_file = (
            get_dist_snapshots_path(dist) if not skip_snapshots else '/dev/null'
        )

        if dist.dist < 'v2023':
            origin = 'Apertis'
            label = f'Apertis {dist.dist} repository'
        else:
            origin = label = f'shared/apertis/public/apertis {dist.dist}'

        extra_args = [
            f'--apt-snapshots={snapshots_file}',
            f'--aptly-snapshot-template={aptly_repo}/default/{{apt-snapshot}}',
            f'--publish-origin={origin}',
            f'--publish-label={label}',
        ]
    elif dist.path == 'debian/buster':
        aptly_repo = 'apertis:infrastructure'
        extra_args = ['--static-aptly-repo-name']
    else:
        sys.exit(f'Unknown dist {dist}')

    cmd = [
        'apt2aptly',
        f'{aptly_repo}/default',
        f'{BASE_URL}/{dist.group}',
        dist.dist,
        '--max-parallel=16',
        f'--publish-to={dist.group}',
        f'--publish-gpg-key={gpg_key}',
        '--update-existing-repo-publish',
        '--create-aptly-repo',
        *extra_args,
    ]
    if dry_run:
        cmd.append('--dry-run')

    print(f'+ {" ".join(map(shlex.quote, cmd))}')
    return subprocess.run(cmd, env=env)


app = typer.Typer()


@app.command()
def download_snapshots_lists(force_update: bool = False) -> None:
    snapshots_dir = ROOT_DIR / 'snapshots'
    snapshots_dir.mkdir(exist_ok=True)

    for dist in list_dist_entries():
        if dist.group != DistEntry.APERTIS_GROUP:
            print(f'NOTE: Skipping snapshots for non-apertis dist {dist}')
            continue

        path = get_dist_snapshots_path(dist)

        tmp = path.with_suffix('.tmp')
        tmp.unlink(missing_ok=True)

        if path.exists() and not force_update:
            print(f'NOTE: Skipping {dist}, already exists')
            continue

        if dist.dist.startswith('v2021'):
            print(f'NOTE: Skipping snapshots for old dist {dist}')
            path.touch(exist_ok=True)
            continue

        print(f'Scanning {dist}...')

        snapshot_resources = download_dist_snapshots_list(dist)
        release_resources = download_dist_releases_list(dist)
        snapshots = []

        with ThreadPoolExecutor() as executor:
            release_snapshots = set()
            for (release, env) in executor.map(
                lambda resource: (
                    resource.name,
                    download_content(
                        f'{resource.url}/meta/build-env-apertis-image-recipes.txt'
                    ),
                ),
                release_resources,
            ):
                matches = re.findall(
                    r'APT_SNAPSHOT=(\d{8}T\d{6}Z)',
                    env.decode('utf-8'),
                )
                assert matches, release

                print(f'Saving release {release} snapshot {matches[0]}')
                release_snapshots.add(matches[0])

            snapshot_resources = [
                resource
                for resource in snapshot_resources
                if resource.name in release_snapshots
                or resource.name >= SNAPSHOTS_CUTOFF
            ]

            if dist.dist == 'v2022-updates':
                # v2022.1 uses the snapshot 20220525T110817Z, but that snapshot's
                # v2022-updates has a conflicting build of libghc-assoc-dev
                # 1.0.2-1co1bv2022.0b1, so skip the v2022-updates snapshot.
                snapshot_resources = [
                    resource
                    for resource in snapshot_resources
                    if resource.name != '20220525T110817Z'
                ]
            if dist.dist == 'v2023-updates':
                # Work around both v2023-updates and v2024dev* having different variants
                # of the same package/version golang-github-ulikunitz-xz
                # 0.5.6-2+apertis1.
                snapshot_resources = [
                    resource
                    for resource in snapshot_resources
                    if resource.name < '20230706T155027Z'
                    or resource.name > '20230814T162205Z'
                ]

            for (snapshot, response) in executor.map(
                lambda resource: (
                    resource.name,
                    REQUESTS_SESSION.head(f'{resource.url}/Release'),
                ),
                snapshot_resources,
            ):
                if response.status_code == 404:
                    print(f'WARNING: ignoring empty snapshot {dist}/{snapshot}')
                else:
                    print(f'Found snapshot {dist}/{snapshot}')
                    snapshots.append(snapshot)

        with tmp.open('w') as fp:
            for snapshot in sorted(snapshots):
                print(snapshot, file=fp)
        tmp.rename(path)


@app.command()
def test_diff_dists(first_url: str, second_url: str) -> None:
    reporter = DiffReporter(sys.stdout)
    diff_dists(reporter, first_url, second_url)


@app.command()
def dry_run(dist: str, skip_snapshots: bool = False) -> None:
    result = run_apt2aptly(
        dist=DistEntry.parse(dist),
        dry_run=True,
        skip_snapshots=skip_snapshots,
    )
    sys.exit(result.returncode)


@app.command()
def run_migration(
    limit: Annotated[int, typer.Option(min=0)],
    filters: Annotated[Optional[list[str]], typer.Option('--filter')] = None,
    force_update: bool = False,
    skip_reports: bool = False,
    skip_snapshots: bool = False,
):
    reports = ROOT_DIR / 'reports'
    reports.mkdir(exist_ok=True)

    dest_url = os.environ.get('APTLY_REPO_URL', DEFAULT_APTLY_REPO_URL)

    migrations_run = 0

    dists = list_dist_entries()
    for dist in dists:
        if filters:
            if not any(fnmatch.fnmatch(dist.dist, pattern) for pattern in filters):
                print(f'Skipping excluded dist {dist}...')
                continue

        dist_reports = reports / dist.path
        dist_reports.mkdir(exist_ok=True, parents=True)

        main_report = dist_reports / 'repo.txt'
        if main_report.exists() and not force_update:
            print(f'Repo report for {dist} already exists, skipping migration...')
            continue

        result = run_apt2aptly(dist=dist, dry_run=False, skip_snapshots=skip_snapshots)
        if result.returncode != 0:
            sys.exit('apt2aptly failed')

        dist_url_subpath = f'{dist.group}/dists/{dist.dist}'

        if not skip_reports:
            if dist.group == DistEntry.APERTIS_GROUP and not skip_snapshots:
                snapshot_reports = dist_reports / 'snapshots'
                snapshot_reports.mkdir(exist_ok=True)

                with ProcessPoolExecutor() as executor:
                    futures = []

                    for snapshot in list_dist_snapshots(dist):
                        snapshot_report = (snapshot_reports / snapshot).with_suffix(
                            '.txt'
                        )
                        if snapshot_report.exists():
                            print(
                                f'Snapshot report for {dist}/{snapshot} already exists'
                                + ', skipping diff...'
                            )
                            continue

                        snapshot_url_subpath = (
                            f'{dist_url_subpath}/snapshots/{snapshot}'
                        )

                        futures.append(
                            executor.submit(
                                task_diff_dists_to_file,
                                heading=f'{dist}/{snapshot}',
                                path=snapshot_report,
                                url_1=f'{BASE_URL}/{snapshot_url_subpath}',
                                url_2=f'{dest_url}/{snapshot_url_subpath}',
                            )
                        )

                    wait(futures)

            task_diff_dists_to_file(
                heading=dist.path,
                path=main_report,
                url_1=f'{BASE_URL}/{dist_url_subpath}',
                url_2=f'{dest_url}/{dist_url_subpath}',
            )

        migrations_run += 1
        if limit > 0 and migrations_run >= limit:
            print('Reached migrations limit, exiting...')
            sys.exit()


if __name__ == '__main__':
    app()
