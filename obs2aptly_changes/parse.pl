#!/usr/bin/env perl

use v5.36;
use warnings;
use strict;

use File::Basename;
use JSON::PP;

my $script_dir = dirname __FILE__;

open my $log, "<$script_dir/all.log";

my %result = (
  added => [],
  removed_keys => [],
  removed_pkgs => [],
  obs_downgrades => [],
  conflicts => [],
  fatal_errors => [],
  missing_components => [],
);

my $dist = '';
my $component = '';
my $arch = '';

my @added_files = ();
my @removed_keys = ();
my %removed_pkgs = ();
my @obs_downgrades = ();
my @conflicts = ();
my @fatal_errors = ();

sub finish_arch() {
  my %base = (dist => $dist, component => $component, arch => $arch);

  foreach my $file (@added_files) {
    push @{$result{added}}, { %base, file => $file };
  }
  foreach my $key (@removed_keys) {
    push @{$result{removed_keys}}, { %base, key => $key };
  }
  foreach my $pkg (keys %removed_pkgs) {
    my $keys = $removed_pkgs{$pkg};
    push @{$result{removed_pkgs}}, { %base, pkg => $pkg, keys => $keys };
  }
  foreach my $downgrade (@obs_downgrades) {
    push @{$result{obs_downgrades}}, { %base, %$downgrade };
  }

  @added_files = ();
  @removed_keys = ();
  %removed_pkgs = ();
  @obs_downgrades = ();
}

sub finish_component() {
  finish_arch;

  my %base = (dist => $dist, component => $component);

  foreach my $conflict (@conflicts) {
    push @{$result{conflicts}}, { %base, %$conflict };
  }
  foreach my $error (@fatal_errors) {
    push @{$result{fatal_errors}}, { %base, %$error };
  }

  @conflicts = ();
  @fatal_errors = ();
}

sub clean_ansi($line) {
  # https://superuser.com/questions/380772/removing-ansi-color-codes-from-text-stream
  $line =~ s/\e\[[0-9;]*m//g;
  $line
}

sub split_dist_component($line) {
  # https://superuser.com/questions/380772/removing-ansi-color-codes-from-text-stream
  $line =~ s/\e\[[0-9;]*m//g;
  my @results = $line =~ /^(\S+) (\S+)\|(.*)$/ or die "invalid line: $line";
  @results
}

sub trim_path($file) {
  $file =~ s|
    ^/srv/build.collabora.com/backend-storage/repos/apertis:/
    $dist:/$component/default/
  ||x or die "unexpected path '$file'";
  $file
}

sub next_line {
  my $line = <$log>;
  chomp $line;
  my ($xdist, $xcomponent, $xline) = split_dist_component clean_ansi($line);
  if ($dist ne $xdist || $component ne $xcomponent) {
    die "unexpected $dist/$xdist -> $component/$xcomponent"
  }

  $xline
}

sub skip_until_spantrace_end {
  my $line;
  my $i = 0;
  until (($line = next_line)
            =~ /Run with RUST_BACKTRACE=full to include source snippets/) {
    $i += 1;
    die "unexpectedly long span trace" if $i > 30;
  }
}

while (my $line = <$log>) {
  chomp $line;
  my ($new_dist, $new_component, $line) = split_dist_component clean_ansi($line);
  if ($new_dist ne $dist || $new_component ne $component) {
    finish_component;

    $dist = $new_dist;
    $component = $new_component;
    $arch = '';
  }

  if (my ($new_arch) = $line =~ /== Syncing (.*) ==/) {
    if ($new_arch eq 'arch indep packages') {
      $new_arch = 'all';
    } elsif ($new_arch eq 'sources') {
      $new_arch = 'source';
    } elsif ($new_arch !~ /^(amd64|arm64|armhf)$/) {
      die "invalid arch '$arch'";
    }

    finish_arch if $arch ne '';
    $arch = $new_arch;
    next;
  }

  if ($line =~ /Looking for existing packages in the pool/) {
    finish_arch;
    $arch = '';
    next;
  }

  if ($line =~ /Error:/) {
    my $lineno = $.;
    my @messages = ();
    until (($line = next_line) eq '') {
      my ($message) = $line =~ /(?:\d+): (.*)/ or die "invalid error line: $line";
      push @messages, $message;
    }
    push @fatal_errors, { lineno => $lineno, messages => \@messages };
    skip_until_spantrace_end;
    next;
  } elsif ($line =~ /ERROR.*reuse_existing_pool_packages/) {
    $line = next_line;
    if (my ($file, $key) = $line
          =~ /Package '(.*)' already exists with different key '(.*)'/) {
      push @conflicts, { incoming_file => trim_path($file), existing_key => $key };
    } else {
      die "invalid error line: $line";
    }
    skip_until_spantrace_end;
    next;
  }

  next if $line =~ /Using package/;
  next if $line =~ /== Actions calculated ==/;

  die "unset arch on line: $line" if $arch eq '';
  next if $line =~ /== Changes for/;
  next if $line =~ /Keeping .*/;
  next if $line =~ /Looking for existing packages/;

  if (my ($removed_pkg) = $line =~ /== No longer in origin: (\S+) ==/) {
    $removed_pkgs{$removed_pkg} = [];
  } elsif (my ($removed_key) = $line =~ /Remove from aptly: (.*)$/) {
    my ($removed_pkg) = $removed_key =~ /^P(?:\S+) (\S+)/
      or die "invalid removed key '$removed_key'";
    if (my $keys = $removed_pkgs{$removed_pkg}) {
      push @$keys, $removed_key;
    } else {
      push @removed_keys, $removed_key;
    }
  } elsif (my ($file) = $line =~ /(?:Add dsc|Adding deb): (.*)/) {
    push @added_files, trim_path($file);
  } elsif (my ($obs, $key) = $line =~ /WARN.*: (\S+) older than (.*) in aptly/) {
    $obs = trim_path $obs;

    my ($key_arch, $key_pkg, $key_version) = $key =~ /^P(\S+) (\S+) (\S+)/
      or die "invalid key '$key'";
    my ($obs_version) = $obs =~ /.*\/\Q$key_pkg\E_(.*)_$key_arch\.deb/
      or die "invalid obs path '$obs'";
    push @obs_downgrades, {
      pkg => $key_pkg,
      obs => $obs, obs_version => $obs_version,
      key => $key, key_version => $key_version,
    };
  } else {
    die "invalid line: $line";
  }
}

finish_component;

my $json = JSON::PP->new->allow_nonref
  ->canonical
  ->indent->indent_length(2)
  ->space_after;

my $json_path = "$script_dir/changes.json";
open my $json_fp, ">$json_path.tmp";
say $json_fp $json->encode(\%result);
rename "$json_path.tmp", $json_path;
