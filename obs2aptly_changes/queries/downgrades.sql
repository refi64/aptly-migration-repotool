select dist, component, arch, source,
  any_value(key_version) as 'repo version',
  any_value(obs_version) as 'OBS version',
  list(pkg) as 'binary packages'
from obs_downgrades
join bin2src on (pkg = "binary")
group by (dist, component, arch, source);
