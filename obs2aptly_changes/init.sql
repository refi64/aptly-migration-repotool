create table changes as
  select * from 'obs2aptly_changes/changes.json';
create table added as select t.* from (select unnest(added) as t from changes);
create table conflicts as select t.* from (select unnest(conflicts) as t from changes);
create table fatal_errors as select t.* from (select unnest(fatal_errors) as t from changes);
create table obs_downgrades as select t.* from (select unnest(obs_downgrades) as t from changes);
create table removed_keys as select t.* from (select unnest(removed_keys) as t from changes);
create table removed_pkgs as select t.* from (select unnest(removed_pkgs) as t from changes);

create table bin2src as from read_csv('obs2aptly_changes/bin2src.csv', header=true, auto_detect=true);

.maxrows 100
