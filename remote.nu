#!/usr/bin/env nu

let LIMA_BUILDDIR = '/tmp/lima/aptly-rest-tools-build'
let CARROT_SSH = 'refi64R@carrot.collabora.co.uk'

def run [args] {
  let saved_in = $in
  print -e ($args | prepend '+' | str join ' ')
  do -c { $saved_in | run-external ($args | get 0) ($args | range 1..) }
}

def kubectl-ns [args] {
  run ([kubectl -n $env.KUBENS] ++ $args)
}

def kubectl-ns-exec [args] {
  kubectl-ns ([exec -it repotool --] ++ $args)
}

def build-aptly-rest-tools [distro ...targets] {
  let lima_builddir = $'($LIMA_BUILDDIR)/($distro)'
  let local_builddir = $'build/($distro)'
  let target_args = ($targets | each {|t| [-p, $t]} | flatten)

  mkdir $lima_builddir $local_builddir
  run [rsync -a --info=progress2 --delete $'($local_builddir)/' $lima_builddir]
  run ([podman
    --remote run --security-opt label=disable --rm -it --arch x86_64
    -v $'(pwd)/../aptly-rest-tools:/source' -w /source
    -v $'($lima_builddir):/build'
    --env=CARGO_HOME=/build/home
    $'docker.io/rust:1.71-($distro)'
    cargo build --release --target-dir /build/target] ++ $target_args)
  run [rsync -a --info=progress2 --delete $'($lima_builddir)/' $local_builddir]
}

def obs2aptly-commands [dist components --apply: bool] {
  let components = if ($components | is-empty) {
    if $dist == 'debian/buster' {
       ['tools']
    } else {
      http get $'https://repositories.apertis.org/apertis/dists/($dist)/Release' |
        parse -r 'Components: (?<c>.*)' | get 0.c | split row ' '
    }
  } else {
    $components
  }

  $components | each {|component|
    let obs_repo = if $dist == 'debian/buster' {
      'apertis:infrastructure'
    } else {
      $'apertis:($dist):($component)'
    }
    let obs_path = ([
      /srv/build.collabora.com/backend-storage/repos
      ($obs_repo | str replace -a ':' ':/')
    ] ++ (if $dist == 'debian/buster' {
        ['buster']
      } else {
        ['default']
      }) | str join '/')

    mut obs2aptly = [./obs2aptly $'($obs_repo)/default' $obs_path]
    if not $apply {
      $obs2aptly ++= [--dry-run]
    }

    $"($obs2aptly | str join ' ') 2>&1 | sed 's,^,($dist) ($component)|,'"
  }
}

def main [] {}

def 'main create' [
    --update: bool
    --namespace (-n): string = 'aptly-test-manual'
    --gpg-key (-g): string = '0BC0002B26BAFF8F'] {
  with-env [KUBENS $namespace] {
    if not $update {
      kubectl-ns [run repotool
        --image=debian:bookworm
        --env=APTLY_API_URL=http://aptly:8080/
        --env=APTLY_REPO_URL=http://aptly:80
        $'--env=APTLY_GPG_KEY=($gpg_key)'
        --command
        # https://stackoverflow.com/a/35770783/2097780
        -- bash -c 'trap : TERM INT; sleep infinity & wait']
      sleep 5sec
    }

    kubectl-ns-exec [apt -y update]
    kubectl-ns-exec [apt -y install libssl3 python3 python3-apt python3-pip]
    kubectl-ns-exec ([python3 -m pip install --break-system-packages]
      ++ (open requirements.txt | lines))
  }
}

def 'main destroy' [--namespace (-n): string = 'aptly-test-manual'] {
  with-env [KUBENS $namespace] {
    kubectl-ns [delete pod/repotool]
  }
}

def 'main push' [--namespace (-n): string = 'aptly-test-manual'] {
  with-env [KUBENS $namespace] {
    kubectl-ns-exec [mkdir -p /work]

    build-aptly-rest-tools bookworm aptlyctl apt2aptly

    for tool in [apt2aptly aptlyctl] {
      kubectl-ns [cp
        $'($LIMA_BUILDDIR)/bookworm/target/release/($tool)'
        repotool:/usr/local/bin/]
    }

    for path in [repotool.py dists.txt snapshots] {
      kubectl-ns [cp $path repotool:/work/]
    }

    kubectl-ns-exec [ln -sf /work/repotool.py /usr/local/bin/repotool]
  }
}

def 'main push-repotool' [--namespace (-n): string = 'aptly-test-manual'] {
  with-env [KUBENS $namespace] {
    kubectl-ns [cp repotool.py repotool:/work/]
  }
}

def 'main restore-reports' [--namespace (-n): string = 'aptly-test-manual'] {
  with-env [KUBENS $namespace] {
    kubectl-ns-exec [test ! -d /work/reports]
    kubectl-ns-exec [mkdir -p /work]
    kubectl-ns [cp reports repotool:/work/reports]
  }
}

def 'main pull-reports' [--namespace (-n): string = 'aptly-test-manual'] {
  with-env [KUBENS $namespace] {
    rm -rf reports.0
    kubectl-ns [cp repotool:/work/reports reports.0]
    rm -rf reports
    mv reports.0 reports
  }
}

def 'main run' [--namespace (-n): string = 'aptly-test-manual' command: string = ''] {
  with-env [KUBENS $namespace] {
    if $command != '' {
      kubectl-ns-exec [bash -c $command]
    } else {
      kubectl-ns-exec [bash]
    }
  }
}

def 'main carrot push-bin' [] {
  build-aptly-rest-tools bullseye aptlyctl obs2aptly
  run [rsync -a --info=progress2
    $'($LIMA_BUILDDIR)/bullseye/target/release/aptlyctl'
    $'($LIMA_BUILDDIR)/bullseye/target/release/obs2aptly'
    $'($CARROT_SSH):./']
}

def 'main carrot push-env' [] {
  [
    $'export APTLY_API_URL=($env.APTLY_API_URL)',
    $'export APTLY_API_TOKEN=($env.APTLY_API_TOKEN)',
  ] | str join "\n" | run [ssh $CARROT_SSH 'cat > .aptly-env']
}

def 'main carrot obs2aptly' [dist ...components --apply: bool] {
  let commands = if $apply {
    (obs2aptly-commands $dist $components --apply)
  } else {
    (obs2aptly-commands $dist $components)
  }

  run [ssh $CARROT_SSH
    $'. .aptly-env; ulimit -n 50000; ($commands | str join "; ")']
}

def 'main carrot obs2aptly-all' [--apply: bool] {
  let dists = (open dists.txt | lines | where { $in !~ '/' })
  let commands = ($dists | each { |dist|
    print -e $dist
    if $apply {
      obs2aptly-commands $dist [] --apply
    } else {
      obs2aptly-commands $dist []
    }
  } | flatten)

  run [ssh $CARROT_SSH
    $'. .aptly-env; ulimit -n 50000; ($commands | str join "; ")']
}
